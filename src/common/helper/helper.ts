import { toastController } from '@ionic/vue';
import { checkmarkCircle, warning } from 'ionicons/icons';

export const helper = {
    async presentToast(msg: any, config?: any) {
        const toast = await toastController.create({
            header: msg.header,
            message: msg.msg,
            duration: 1500,
            position: config?.position || 'bottom',
            color: config?.color || 'secondary',
            buttons: [{
                side: 'end' || 'start',
                icon: config.error ? warning : checkmarkCircle
            }],
            mode: 'ios'
        });
        await toast.present();
    },
}
