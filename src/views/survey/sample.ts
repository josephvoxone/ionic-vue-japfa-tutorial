export const triviaSample = [
    {
        "category": "Geography",
        "id": "62602cfc4b176d54800e3c76",
        "correctAnswer": "Japan",
        "incorrectAnswers": [
            "Philippines",
            "India",
            "China"
        ],
        "question": "Tokyo is a city in which country?",
        "tags": [
            "cities",
            "general_knowledge",
            "geography"
        ],
        "type": "Multiple Choice",
        "difficulty": "easy",
        "regions": []
    },
    {
        "category": "Geography",
        "id": "6233853962eaad73716a8c59",
        "correctAnswer": "The Richter Scale ",
        "incorrectAnswers": [
            "The Geiger Scale",
            "The Scoville Scale",
            "The Bristol Scale"
        ],
        "question": "What Scale Is Used To Measure The Intensity Of An Earthquake",
        "tags": [
            "geology",
            "words",
            "geography"
        ],
        "type": "Multiple Choice",
        "difficulty": "easy",
        "regions": []
    },
    {
        "category": "Geography",
        "id": "62602eb74b176d54800e3cf0",
        "correctAnswer": "Johannesburg",
        "incorrectAnswers": [
            "Algiers",
            "Nairobi",
            "Luanda"
        ],
        "question": "Which of these cities is in South Africa?",
        "tags": [
            "cities",
            "africa"
        ],
        "type": "Multiple Choice",
        "difficulty": "easy",
        "regions": []
    },
    {
        "category": "Geography",
        "id": "62602d474b176d54800e3c8c",
        "correctAnswer": "Moscow",
        "incorrectAnswers": [
            "Zürich",
            "Barcelona",
            "Rotterdam"
        ],
        "question": "Which of these cities is in Russia?",
        "tags": [
            "cities",
            "europe",
            "russia",
            "geography"
        ],
        "type": "Multiple Choice",
        "difficulty": "easy",
        "regions": []
    },
    {
        "category": "Geography",
        "id": "623b578afd6c701a92118364",
        "correctAnswer": "Egypt",
        "incorrectAnswers": [
            "Indonesia",
            "Peru",
            "Mexico"
        ],
        "question": "In which country would you find the Pyramids of Giza?",
        "tags": [
            "tourist_attractions",
            "classics",
            "general_knowledge",
            "geography"
        ],
        "type": "Multiple Choice",
        "difficulty": "easy",
        "regions": []
    }
]