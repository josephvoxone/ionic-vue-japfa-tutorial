import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'survey',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
